## DEFAULTS ##

COMPLETION_WAITING_DOTS="true"
ENABLE_CORRECTION="true"
grep='grep  --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn}'
HIST_STAMPS="mm/dd/yyyy"
HISTFILESIZE=1000000000 HISTSIZE=1000000
ls='ls --color=tty'
ZSH="$HOME/.oh-my-zsh"
ZSH_THEME="dpoggi"
source $ZSH/oh-my-zsh.sh

## PLUGIN ##

plugins=(
brew
colorize
pip
zsh-completions
)

## EXPORT ##
export ANSIBLE_NOCOWS=1
export EDITOR="vim"
export fpath=(/usr/local/share/zsh-completions $fpath)
export LANG=en_US.UTF-8
export MANPATH="/usr/local/man:$MANPATH"
export PATH="/usr/bin/gem:$PATH"
export PATH="/usr/local/opt/openssl/bin:$PATH"
export PATH="/usr/local/opt/python/libexec/bin:$PATH"
export PATH="/usr/local/sbin:$PATH"
export PATH=/usr/local/bin:/usr/local/sbin:$PATH
export SSH_KEY_PATH="~/.ssh/id_rsa.pub"
export superexit=$(ps -ax | awk '! /awk/ && /Terminal/ { print $1}')
export UPDATE_ZSH_DAYS=7
export zsh="$HOME/.oh-my-zsh"
export ZSH_HIGHLIGHT_HIGHLIGHTERS_DIR=/usr/local/share/zsh-syntax-highlighting/highlighters

if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
fi

## ALIAS ##
alias ap='ansible-playbook -i ../hosts'
alias avd='ansible-vault decrypt '
alias ave='ansible-vault encrypt '
alias c='clear'
alias cats='cat ~/.ssh/id_rsa.pub'
alias certexp='"openssl x509 -enddate -noout -in "/etc/letsencrypt/live/{{ ansible_fqdn }}/fullchain.pem" | cut -c 10-"'
alias cgit='cd `git rev-parse --show-toplevel`'
alias chownme='sudo chown -R $whoami:$whoami'
alias count='find . -type f | wc -l'
alias cp='rsync -ah --stats --info=progress2'
alias cs='printf "Filename: "; read filename && ln -s $filename $HOME/Desktop/$filename'
alias gemup='gem clean && gem update'
alias gup='git add . && git commit -m "$(date)" && git push'
alias hogs='du -ah / | sort -rn | head -n 10'
alias l='colorls --group-directories-first --almost-all'
alias ll='colorls --group-directories-first --almost-all --long' # detailed list view
alias lt='ls --human-readable --size -1 -S --classify'
alias mailt='mail -s "cmd line email test from $hostname" nathanielloveland@gmail.com </dev/null'
alias mnt="mount | awk -F' ' '{ printf \"%s\t%s\n\",\$1,\$3; }' | column -t | egrep ^/dev/ | sort"
alias nods="sudo find . -name ".DS_Store" -delete"
alias pipup="pip list --outdated --format=freeze | grep -v '^\-e' | cut -d = -f 1  | xargs -n1 pip install -U"
alias pm='postfix flush && mailq | mailq >> /tmp/mailq.log && postsuper -d ALL deferred'
alias q='kill -9 $superexit'
alias rmedir='find . -depth -type d -empty -exec rmdir {} \;'
alias rmtmp="find . -type f -name '._*' -delete"
alias s='clear && source ~/.zshrc'
alias sgit='cd `git rev-parse --show-toplevel` && git checkout master && git pull'
alias ttime='ls -t -1'
alias yumup='yum autoremove -y && yum clean all -y && yum update -y && yum upgrade -y'
alias z="vim ~/.zshrc"

function finds() {
	echo
	echo "This utility recursively finds an inputted string from current directory." | lolcat
	bash -c 'read -p "Find: " fstring;'
	find . -type f -print0 2>/dev/null | xargs -0 -n 10 grep -i $fstring 2>/dev/null
}

function cl() {
    DIR="$*";
        # if no DIR given, go home
        if [ $# -lt 1 ]; then
                DIR=$HOME;
    fi;
    builtin cd "${DIR}" && \
    # use your preferred ls command
        ls -F --color=auto
}

function ren () {
	echo
	echo "*------------*" | lolcat
	echo "|SFCO Renamer|" | lolcat
	echo "*------------*" | lolcat
	echo
	echo "This utility recursively renames all" | lolcat
	echo "file extensions within current directory" | lolcat
	echo "based on user input." | lolcat
	echo
	bash -c 'read -p "Extension to Rename?: " rn1;'
	bash -c 'read -p "Rename $rn1 to?: " rn2;'
	find . -name "*.$rn1" -exec rename 's/\.'$rn1'$/.'$rn2'/' '{}' \;
	echo
	echo "Done!"
	echo
}

function bmtool () {
	echo "SFCO Benchmark Util" | lolcat
	echo "This utility will test read and write speeds based on user disk input"
	echo

	disksavail=$(diskutil list | grep /dev/disk)
	IFS=': ' read -r -a array <<< "$disksavail"
	echo

	echo "Available Disks:" | lolcat
	echo "----------------" | lolcat
	echo "${array[@]}"
	echo

	read -p "Test local disk speed? (Y/N): " loctest

	if [[ $loctest == "Y" ]]; then
		echo $ptf | lolcat
		echo "Local Disk Read Speed:"
		readspeedfrom=$(sudo dd if=tstfile bs=1024k of=/dev/null count=1024)
		echo "Local Disk Write Speed:"
		writespeedfrom=$(sudo time dd if=/dev/zero bs=1024k of=tstfile count=1024)
		echo
		read -p "Additional disk to benchmark? (Y/N): " dpath
			if [[ $dpath == "N" ]]; then
				echo "Done!"
				exit
			fi
	else
		read -p "Input Disk to Benchmark: " dpath
	fi

	echo
	echo "Benchmarking $dpath.." | lolcat
	echo
	echo "Read Speed:"
	readspeedto=$(sudo dd if=tstfile bs=1024k of=$dpath count=1024)
	echo "Write Speed:"
	writespeedto=$(sudo time dd if=$dpath bs=1024k of=tstfile count=1024)
	echo
	echo "Done!"
}
## PROFILE ##

fortune -s | cowsay -f tux | lolcat -s 64
echo