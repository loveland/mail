echo 'y' | /root/.pyenv/bin/pyenv install {{ py_ver }} > /tmp/.build_data/log/{{ py_ver }}.log;
git clone https://github.com/yyuu/pyenv-virtualenv.git ~/.pyenv/plugins/pyenv-virtualenv
echo >> /tmp/.build_data/log/{{ py_ver }}.log;
/root/.pyenv/bin/pyenv global {{ py_ver }} >> /tmp/.build_data/log/{{ py_ver }}.log;
exit