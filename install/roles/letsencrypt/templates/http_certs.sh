fuser -k 80/tcp
/root/.pyenv/shims/certbot certonly --standalone --preferred-challenges http -d {{ ansible_fqdn }} -m {{ admin_email }}