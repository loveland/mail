- name: ensure no yum lock
  file:
    path: /run/yum.pid
    state: absent

- name: ensure no conflict pkgs
  yum:
    name: python-urllib3
    state: absent

- name: ensure dep pkgs
  yum:
    state: latest
    name:
      - epel-release.noarch
      - yum-utils

- name: ensure upgraded pip
  pip:
    executable: /root/.pyenv/shims/pip3
    state: latest
    name: pip

- name: ensure certbot and deps
  pip:
    executable: /root/.pyenv/shims/pip3
    state: latest
    name:
      - acme
      - boto
      - certbot
      - certbot-apache
      - openssl-python
      - pyOpenSSL
      - s3transfer-python3
  ignore_errors: yes

- name: ensure certbot and deps
  pip:
    state: latest
    name:
      - acme
      - boto
      - certbot
      - certbot-apache
      - certifi
      - chardet
      - idna
  ignore_errors: yes
  delegate_to: localhost
  run_once: true

- name: start and enable atd
  service:
    name: atd
    enabled: yes
    state: started

- name: install letsencrypt from web repo for automated cert renewal
  git:
    repo: 'https://github.com/letsencrypt/letsencrypt'
    dest: /opt/letsencrypt
    clone: yes
    update: yes
    force: yes
  when: letsencrypt_full_install == true

- name: ensure new letsencrypt host directory
  file:
    path: "{{ ler53_cert_dir }}"
    owner: root
    mode: 0755
    state: absent
  when: newcerts == true

- name: ensure letsencrypt host directory
  file:
    path: "{{ ler53_cert_dir }}"
    state: directory
    owner: root
    mode: 0755

- name: ensure dir exists
  file:
    path: /etc/ssl/{{ ansible_fqdn }}
    state: directory

- name: fix logjam attack by generating better Diffie-Hellman encryption #ensure smtpd_tls_dh1024_param_file = /etc/ssl/certs/dhparams.pem is set in main.cf
  command: openssl dhparam -out /etc/ssl/{{ ansible_fqdn }}/dhparams.pem 2048 creates=/etc/ssl/{{ ansible_fqdn }}/dhparams.pem

- name: generate the private key
  command: "openssl genrsa -out \"{{ ler53_cert_dir }}/{{ ler53_key_file_name }}\" {{  ler53_key_size  }}"
  args:
    creates: "{{ ler53_cert_dir }}/{{ ler53_key_file_name }}"

- name: set the private key file permissions
  file:
    path: "{{ ler53_cert_dir }}/{{ ler53_key_file_name }}"
    owner: "{{ ler53_cert_files_owner }}"
    group: "{{ ler53_cert_files_group }}"
    mode: "{{ ler53_cert_files_mode }}"

- name: check if the CSR exists
  stat:
    path: "{{ ler53_cert_dir }}/{{ ler53_csr_file_name }}"
  register: ler53_csr_stat

- name: create the OpenSSL configuration file for the CSR
  template:
    src: openssl-request.conf.j2
    dest: "{{ ler53_cert_dir }}/openssl-request.conf"
    owner: root
    mode: 0600
  when: not ler53_csr_stat.stat.exists

- name: generate the CSR
  command: "openssl req -new -sha256 -subj \"/CN={{ ler53_cert_common_name }}\" -config \"{{ ler53_cert_dir }}/openssl-request.conf\"  -key \"{{ ler53_cert_dir }}/{{ ler53_key_file_name }}\" -out \"{{ ler53_cert_dir }}/{{ ler53_csr_file_name }}\""
  args:
    creates: "{{ ler53_cert_dir }}/{{ ler53_csr_file_name }}"

- name: set the CSR file permissions
  file:
    path: "{{ ler53_cert_dir }}/{{ ler53_csr_file_name }}"
    owner: "{{ ler53_cert_files_owner }}"
    group: "{{ ler53_cert_files_group }}"
    mode: "{{ ler53_cert_files_mode }}"

- name: delete the OpenSSL configuration file for the CSR
  file:
    path: "{{ ler53_cert_dir }}/openssl-request.conf"
    state: absent

- name: "create the {{ ler53_account_key_dir }} directory"
  file:
    path: "{{ ler53_account_key_dir }}"
    owner: root
    mode: 0700
    state: directory

- name: generate the Let's Encrypt account key
  command: "openssl genrsa -out \"{{ ler53_account_key_dir }}/{{ ler53_account_key_file_name }}\" {{ ler53_account_key_size }}"
  args:
    creates: "{{ ler53_account_key_dir }}/{{ ler53_account_key_file_name }}"

- name: set the Let's Encrypt account key file permissions
  file:
    path: "{{ ler53_account_key_dir }}/{{ ler53_account_key_file_name }}"
    owner: root
    mode: 0600
  when: letsencrypt_full_install == false

- name: retrieve certs via lets encrypt with dns validation. if dns fail then use http validation.
  block:

    - name: initiate the Let's Encrypt challenge
      acme_certificate:
        acme_directory: https://acme-v02.api.letsencrypt.org/directory
        acme_version: 2
        challenge: dns-01
        terms_agreed: yes
        account_key: "{{ ler53_account_key_dir }}/{{ ler53_account_key_file_name }}"
        csr: "{{ ler53_cert_dir }}/{{ ler53_csr_file_name }}"
        dest: "{{ ler53_cert_dir }}/{{ ler53_cert_file_name }}"
        account_email: "{{ admin_email }}"
        remaining_days: "{{ ler53_cert_remaining_days_before_renewal }}"
      register: lets_encrypt_challenge

    - name: create the DNS records for the challenge
      route53:
        command: create
        zone: "{{ zone }}"
        record: "_acme-challenge.{{  item.key  }}"
        type: TXT
        ttl: 5
        value: "\"{{  item.value['dns-01']['resource_value']  }}\""
        aws_access_key: "{{ ler53_aws_access_key }}"
        aws_secret_key: "{{ ler53_aws_secret_key }}"
        overwrite: yes
        wait: yes
      with_dict: "{{  lets_encrypt_challenge['challenge_data'] | default({})  }}"
      delegate_to: localhost

    - name: validate the Let's Encrypt challenge
      acme_certificate:
        acme_directory: https://acme-v02.api.letsencrypt.org/directory
        acme_version: 2
        terms_agreed: yes
        challenge: dns-01
        account_key: "{{ ler53_account_key_dir }}/{{ ler53_account_key_file_name }}"
        csr: "{{ ler53_cert_dir }}/{{ ler53_csr_file_name }}"
        dest: "{{ ler53_cert_dir }}/{{ ler53_cert_file_name }}"
        account_email: "{{ admin_email }}"
        data: "{{ lets_encrypt_challenge }}"

    - name: delete the DNS records for the challenge
      route53:
        command: delete
        zone: "{{ zone }}"
        record: "_acme-challenge.{{  item.key  }}"
        type: TXT
        ttl: 5
        value: "\"{{  item.value['dns-01']['resource_value']  }}\""
        aws_access_key: "{{ ler53_aws_access_key }}"
        aws_secret_key: "{{ ler53_aws_secret_key }}"
      with_dict: "{{  lets_encrypt_challenge['challenge_data'] | default({})  }}"
      delegate_to: localhost

- name: set the cert file permissions
  file:
    path: "{{ ler53_cert_dir }}/{{ ler53_cert_file_name }}"
    owner: "{{ ler53_cert_files_owner }}"
    group: "{{ ler53_cert_files_group }}"
    mode: "{{ ler53_cert_files_mode }}"

- name: download the Let's Encrypt intermediate CA
  get_url:
    url: https://letsencrypt.org/certs/lets-encrypt-x3-cross-signed.pem
    dest: "{{ ler53_cert_dir }}/{{ ler53_intermediate_file_name }}"
    owner: "{{ ler53_cert_files_owner }}"
    group: "{{ ler53_cert_files_group }}"
    mode: "{{ ler53_cert_files_mode }}"
  register: ler53_intermediate_download_task
  when: ler53_intermediate_download == true

- name: get content of the certificate
  command: "cat {{ ler53_cert_dir }}/{{ ler53_cert_file_name }}"
  register: ler53_certificate_content
  changed_when: false
  when: ler53_intermediate_download == true

- name: get content of the intermediate CA
  command: "cat {{ ler53_cert_dir  }}/{{ ler53_intermediate_file_name }}"
  register: ler53_intermediate_content
  changed_when: false
  when: ler53_intermediate_download == true

- name: create a file with the certificate and intermediate CA concatenated
  copy:
    content: "{{  ler53_certificate_content['stdout'] + '\n' + ler53_intermediate_content['stdout'] + '\n'  }}"
    dest: "{{ ler53_cert_dir }}/{{ ler53_cert_and_intermediate_file_name }}"
    owner: "{{ ler53_cert_files_owner }}"
    group: "{{ ler53_cert_files_group }}"
    mode: "{{ ler53_cert_files_mode }}"
  when: ler53_intermediate_download == true

- name: generate cert renewal
  shell: /opt/letsencrypt/certbot-auto renew --quiet
  when: renew_cert == true
  ignore_errors: yes

- name: crontab cert renewal
  cron:
    name: "ensure certs are kept up to date once every 60 days (At 11:30 on Sunday in every 2nd month from January through December)"
    minute: "30"
    hour: "23"
    day: "7"
    job: "/opt/letsencrypt/certbot-auto renew --quiet"

- name: show when certs expire
  shell: openssl x509 -enddate -noout -in "/etc/letsencrypt/live/{{ ansible_fqdn }}/{{ ansible_fqdn }}.fullchain.pem" | cut -c 10-
  register: certexpire

- name: certexpire command to "{{ profile }}"
  lineinfile:
    dest: "{{ profile }}"
    line: 'alias certexp=openssl x509 -enddate -noout -in "/etc/letsencrypt/live/{{ ansible_fqdn }}/{{ ansible_fqdn }}.fullchain.pem" | cut -c 10-'

- debug:
    var: certexpire.stdout