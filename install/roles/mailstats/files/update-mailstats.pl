#!/usr/bin/perl

# update-mailstats.pl
#
# Copyright Craig Sanders 1999
#
# this script is licensed under the terms of the GNU GPL.

use DB_File;
use File::Tail ;
$debug = 0;

$mail_log = '/var/log/mail.log' ;
$stats_file = '/tmp/.build_data/mail/stats.db' ;

$db = tie(%stats, "DB_File", "$stats_file", O_CREAT|O_RDWR, 0666, $DB_HASH) 
	|| die ("Cannot open $stats_file");

#my $logref=tie(*LOG,"File::Tail",(name=>$mail_log,tail=>-1,debug=>$debug));
my $logref=tie(*LOG,"File::Tail",(name=>$mail_log,debug=>$debug));

while (<LOG>) {
	if (/status=sent/) {
		next unless (/ postfix\//) ;
		# count sent messages
		if (/relay=([^,]+)/o) {
			$relay = $1 ;
			#print "$relay..." ;
		} ;
		if ($relay !~ /\[/o ) {
			$stats{"SENT:$relay"} += 1;
			#print "$relay\n" ;
		} else {
			$stats{"SENT:smtp"} +=1 ;
			#print "smtp\n" ;
		} ;
		$db->sync;
	} elsif (/smtpd.*client=/) {
		# count received smtp messages
		$stats{"RECEIVED:smtp"} += 1;
		$db->sync ;
	} elsif (/pickup.*(sender|uid)=/) {
		# count received local messages
		$stats{"RECEIVED:local"} += 1;
		$db->sync ;
	} ;
} ;

untie $logref ;
untie %stats;

