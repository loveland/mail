#!/usr/bin/perl

$source = `hostname`;
chomp $source ;

#$uptime = `uptime`;
#$uptime =~ s/^.*\s+up\s+// ;
#$uptime =~ s/,\s+\d+\s+users,.*// ;

$uptime = `uptime` ;
chomp $uptime ;
$uptime =~ s/.*up //;
$uptime =~ s/(\d),.*/$1/; 

$datafile = "/tmp/.build_data/mail/mailstat.old";

open(OLD,"<$datafile") ;
while(<OLD>) {
	chomp ;
	($key,$val) = split /=/ ;
	$old{$key} = $val ;
} ;
close(OLD) ;

$mailstats = "/usr/local/bin/mailstats.pl" ;
open(STAT,"$mailstats|") || die "couldn't open pipe to $mailstats: $!" ;
while(<STAT>) {
	chomp ;
	($type,$count) = split ;
	($what,undef) = split(/:/, $type) ;
	$new{$what} += $count ;
} ;
close(STAT) ;
	
print $new{RECEIVED}-$old{RECEIVED},"\n",$new{SENT}-$old{SENT},"\n","$uptime\n$source\n" if ($old{RECEIVED});

# save old stats
open(OLD,">$datafile") ;
foreach (keys %new) {
	print OLD "$_=$new{$_}\n" ;
} ;
close(OLD) ;

