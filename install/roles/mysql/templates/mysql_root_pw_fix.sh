#!/bin/sh
service mysqld stop
mysqld_safe &
sleep 2
mysql -u root <<EOF
use mysql;
ALTER USER 'root'@'localhost' IDENTIFIED BY '{{ mysql_root_pw }}';
flush privileges;
quit
EOF
PID=`ps -ef | grep mysql | grep -v grep | awk '{print $2}'`
kill -9 $PID
sleep 2
service mysqld start