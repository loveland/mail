#/bin/bash
#
# dump all mysql db's into seperate files
#
# loveland
#
for DB in $(mysql -e 'show databases' -s --skip-column-names); do
    'mysqldump $DB > | gzip > $DB.sql.gz'
    echo "Attached are your sql dumps!" | mail -s "Sql dump from {{ ansible_fqdn }}" "{{ admin_email }}" -A $DB.sql.gz
done