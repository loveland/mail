#!/bin/bash
#
## IPTables script utilizing Whitelist & Blacklist
#

WHITELIST=/etc/sysconfig/iptables_whitelist.txt
#BLACKLIST=/etc/sysconfig/iptables_blacklist.txt

#if [ -f "$BLACKLIST" ]; then
#    echo "No blacklist file exists. Creating blank file. $BLACKLIST"
#    touch $BLACKLIST
#else
#    echo "$BLACKLIST exists"
#fi

#
## For port listing reference see http://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers
## To add port range separate by ":" with no spaces.  Ie. "10000:20000"
#

ALLOWED="{{ ipt_ports }}"

#
## To restore using our example we would run iptables-restore
#
## Clear current rules
#

iptables -F
echo 'Clearing tables'
iptables -X
echo 'Deleting user defined chains'
iptables -Z
echo 'Zero chain counters'

iptables -t nat -X
iptables -t mangle -F
iptables -t mangle -X


iptables -P INPUT ACCEPT
echo 'Setting default INPUT policy to ACCEPT'

iptables -P FORWARD ACCEPT
iptables -P OUTPUT ACCEPT

#
##The following rule ensures that established connections are not checked.
##It also allows for things that may be related but not part of those connections such as ICMP.
#

#
##Always allow localhost
#
iptables -I INPUT -s 127.0.0.1 -j ACCEPT

#
##Allow established connections
#
iptables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT

#
##Allow port ping traffic
#
#iptables -A INPUT -s icmp -j ACCEPT

#
## Whitelist
#

for x in `grep -v ^# $WHITELIST | awk '{print $1}'`; do
echo "Permitting $x..."
iptables -A INPUT -s $x -j ACCEPT
done

#
## Blacklist
#

#for x in `grep -v ^# $BLACKLIST | awk '{print $1}'`; do
#echo "Denying $x..."
#iptables -A INPUT -s $x -j DROP
#done

#
## Permitted Ports
#

for port in $ALLOWED; do
echo "Accepting port TCP $port..."
iptables -A INPUT -p tcp --dport $port -j ACCEPT
done

for port in $ALLOWED; do
echo "Accepting port UDP $port..."
iptables -A INPUT -p udp --dport $port -j ACCEPT
done

## Run "iptables -vnL" to ensure the rules are as expected and that your SSH port is correct.\

iptables -P INPUT DROP
iptables -P FORWARD DROP

service iptables save
service iptables reload

iptables -S
