#!/usr/bin/perl
use strict;
our $session;

use DBI;
use POSIX 'strftime';
use IO::All;
use JSON::XS;
#use Data::Dumper;

open(CFG, 'postfix_summ_cfg.json') or die("open: $!");
$/ = undef;
my $cfg = <CFG>;
my $dbcfg = decode_json($cfg);

#{
#	'host' => 'localhost',
#	'database' => 'syslog',
#	'user' => 'syslog',
#	'password' => '...',
#};

my $dsn = "DBI:mysql:database=$dbcfg->{'database'};host=$dbcfg->{'host'}";
my $dbh = DBI->connect($dsn, $dbcfg->{'user'}, $dbcfg->{'password'}) or die "$0: Couldn't connect to database: " . DBI->errstr();

my $file = 'postfix_lastid.txt';
my $last_id = 0;
if (-e $file)
{
	io($file) > $last_id;
}

my $stm = q|SELECT ID, ReceivedAt, Message FROM SystemEvents WHERE ID > ? ORDER BY ID ASC LIMIT 100000|;
my $ul = $dbh->prepare($stm) or die "$0: Couldn't prepare statement $stm: " . $dbh->errstr();
$ul->execute($last_id) or die "$0: SQL Error: ". $dbh->errstr();
my $res = $dbh->selectall_arrayref($ul);

my %post = ();
my @mail = ();
foreach my $r (@$res)
{
	my ($id, $ts, $msg) = @$r;
	$msg =~ s/^\s//;
	if ($msg =~ /^warning: ([A-Z0-9]+)\: (.*)$/)
	{
		my $postid = $1;
		my $info = $2;
		unless (defined $post{$postid})
		{
			push @mail, $postid;
			$post{$postid}->{'ts'} = $ts;
		}
		$post{$postid}->{'status'} = 'warning: '. $info;
	}
	elsif ($msg =~ /^([A-Z0-9]+)\: (.*)$/)
	{
		#print "$msg\n";
		my $postid = $1;
		my $info = $2;
		my @pts;
		if ($info =~ /^uid/)
		{
			@pts = split /\s/, $info;
		}
		else
		{
			@pts = split /\,\s+/, $info;
		}
		foreach my $p (@pts)
		{
			my @kv = split /\=/, $p, 2;
			if (length($kv[0]) && length($kv[1]))
			{
				#print "$kv[0] => $kv[1]\n";
				unless (defined $post{$postid})
				{
					push @mail, $postid;
					$post{$postid}->{'ts'} = $ts;
				}
				$post{$postid}->{$kv[0]} = $kv[1];
			}
		}
		#print "\n";
	}
}

#print Dumper(\%post);

my $select = q|SELECT postfix_id, email_to, email_from, email_size, relay, message_id, ts, last_status FROM email_log WHERE postfix_id=?|;
my $sth1 = $dbh->prepare($select) or die "$0: Couldn't prepare statement $stm: " . $dbh->errstr();
my $insert = q|INSERT INTO email_log (postfix_id, email_to, email_from, email_size, relay, message_id, ts, last_status) VALUES (?, ?, ?, ?, ?, ?, ?, ?)|;
my $sth2 = $dbh->prepare($insert);
my $update = q|UPDATE email_log SET postfix_id=?, email_to=?, email_from=?, email_size=?, relay=?, message_id=?, ts=?, last_status=? WHERE postfix_id=?|;
my $sth3 = $dbh->prepare($update);

my %mapping = (
	'email_to' => 'to',
	'email_from' => 'from',
	'email_size' => 'size',
	'relay' => 'relay',
	'message_id' => 'message-id',
	'ts' => 'ts',
	'last_status' => 'status',
);
foreach my $m (@mail)
{
	#print "$m\n";
	#print Dumper($post{$m});
	my $data = $post{$m};
	$sth1->execute($m) or die "$0: SQL Error: ". $dbh->errstr();
	if ($sth1->rows())
	{
		my $current = $sth1->fetchrow_hashref();
		foreach my $k (keys %$current)
		{
			if (defined($current->{$k}) && ! defined($data->{$mapping{$k }}))
			{
				$data->{$mapping{$k }} = $current->{$k};
			}
		}
		$sth3->execute(
			$m,
			$data->{'to'},
			$data->{'from'},
			$data->{'size'},
			$data->{'relay'},
			$data->{'message-id'},
			$data->{'ts'},
			$data->{'status'},
			$m,
		);
	}
	else
	{
		$sth2->execute(
			$m,
			$data->{'to'},
			$data->{'from'},
			$data->{'size'},
			$data->{'relay'},
			$data->{'message-id'},
			$data->{'ts'},
			$data->{'status'},
		);
	}
}

if (scalar(@$res))
{
	$last_id = $res->[-1]->[0];
	$last_id > io($file);
}

