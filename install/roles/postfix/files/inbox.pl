#!/usr/bin/perl
use strict;
use warnings;

use LWP::UserAgent;
use MIME::Base64 'encode_base64';
use Digest::MD5;
use Storable 'nfreeze';

my $update = 0;

$/ = undef;
my $mail = <STDIN>;
my $lwp = new LWP::UserAgent();
my $dv = encode_base64(nfreeze(\$mail));
if ($update)
{
    my $mds = new Digest::MD5;
    $mds->add($dv);
    my $digest = $mds->hexdigest();
    my $fn = time(). '_'. $digest;
    open(F, '>/home/leads/emails/'. $fn);
    print F $mail;
    close(F);
}
else
{
    #my $url = 'http://www.dialyourleads.com/post_email';
    my $url = 'http://my.{{ newco }}/post_email';
    $lwp->post($url, {
        'data' => $dv,
    });
}

