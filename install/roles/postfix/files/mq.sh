#!/bin/bash
#
# Check mail q #loveland
#

if [ "$(mailq)" != "Mail queue is empty" ]; then
   echo "Attempting re-delivery of mail not sent..." && postqueue -f
elif [ "$(mailq)" == "Mail queue is empty" ]; then
   mailq
fi