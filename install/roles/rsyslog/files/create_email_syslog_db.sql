CREATE TABLE IF NOT EXISTS `SystemEvents` (
  ID bigint unsigned not null auto_increment primary key,
  CustomerID bigint,
  ReceivedAt datetime NULL,
  DeviceReportedTime datetime NULL,
  Facility smallint NULL,
  Priority smallint NULL,
  FromHost varchar(60) NULL,
  Message text,
  NTSeverity int NULL,
  Importance int NULL,
  EventSource varchar(60),
  EventUser varchar(60) NULL,
  EventCategory int NULL,
  EventID int NULL,
  EventBinaryData text NULL,
  MaxAvailable int NULL,
  CurrUsage int NULL,
  MinUsage int NULL,
  MaxUsage int NULL,
  InfoUnitID int NULL ,
  SysLogTag varchar(60),
  EventLogType varchar(60),
  GenericFileName VarChar(60),
  SystemID int NULL
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `email_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `postfix_id` varchar(11) NOT NULL,
  `email_to` varchar(80) DEFAULT NULL,
  `email_from` varchar(80) DEFAULT NULL,
  `email_size` int(11) DEFAULT NULL,
  `relay` varchar(255) DEFAULT NULL,
  `message_id` varchar(255) DEFAULT NULL,
  `ts` datetime DEFAULT NULL,
  `last_status` TEXT DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `postfix_id_1` (`postfix_id`),
  KEY `message_id_1` (`message_id`(32)),
  KEY `ts_1` (`ts`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;