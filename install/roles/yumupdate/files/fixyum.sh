#!/bin/bash
#
# Fix Yum / rpmdb: Thread died in Berkeley DB library
#
# loveland
#

yumdb=/var/lib/rpm/backup

if [ -d "$yumdb" ]; then
    rm -rf /var/lib/rpm/backup
fi

mkdir /var/lib/rpm/backup

cp -a /var/lib/rpm/__db* /var/lib/rpm/backup/
rm -f /var/lib/rpm/__db.[0-9][0-9]*
rpm --quiet -qa
rpm --rebuilddb
yum clean metadata
yum clean all
package-cleanup --oldkernels --count=2 -y
yum distribution-synchronization -y