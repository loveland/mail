#!/bin/bash
#
# Super Yum Update + Clean
#
# loveland
#

yum clean metadata
yum clean all
package-cleanup --leaves --all
package-cleanup --oldkernels --count=2 -y
yum distribution-synchronization -y
yum update -y
yum upgrade - y
yum autoremove -y
dnf clean packages -y